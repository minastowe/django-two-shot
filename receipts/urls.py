from django.urls import path
from .views import receipt_list_list, create_receipt, my_expense_category_list, my_account_list, create_category, create_account


urlpatterns = [
    path("", receipt_list_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", my_expense_category_list, name="category_list"),
    path("accounts/", my_account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
